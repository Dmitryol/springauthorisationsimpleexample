package odmitry.example.authorisation;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
public class MainController {
	
	@Autowired
	UserDao data;
	/**
	 * If user is authorized return welcome page else return back on login page
	 * 
	 */
	@RequestMapping(value = "/**", method = RequestMethod.GET)
	public String login(Locale locale, Model model) {
		
		//Get user's name is SecurityContextHolder 
		String userName= SecurityContextHolder.getContext().getAuthentication().getName();
		
		if(userName.equals("anonymousUser")) {
			return "login";
		}else {
			
            model.addAttribute("username", SecurityContextHolder.getContext().getAuthentication().getName());
			model.addAttribute("users", data.getUsers());
			
			return "welcome";
		}
	
	}
	
}
