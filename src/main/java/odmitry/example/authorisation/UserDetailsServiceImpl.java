package odmitry.example.authorisation;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Realization UserDetailsService 
 * @author Freeman
 *
 */
public class UserDetailsServiceImpl implements UserDetailsService, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	UserDao data;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		UserDetails us;
		
        if(data.isUser(username)) {
        	
        	us=new User(username, data.getUser(username).getPassword(), getRoles(username));
        }else {
        	
        	us=new User(username, data.getUser(username).getPassword(), true, true, false, false, getRoles(username));
        }
			
		return us;
	}
	/**
	 * 
	 * @param username - Login of user
	 * @return role of this user
	 */
	private Set<GrantedAuthority> getRoles(final String username){
		
		GrantedAuthority authority=new GrantedAuthority() {
			
			private static final long serialVersionUID = 1L;

			public String getAuthority() {
				return data.getUser(username).getRole();
			}
		};
		Set<GrantedAuthority> set=new HashSet<GrantedAuthority>();
		set.add(authority);
		
		return set;
		
	}

	
}
