package odmitry.example.authorisation;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
/**
 * Present implementation interface of the UserDao. Uses for getting users form database  
 * @author Freeman
 */
@Component
public class UserDaoDbImpl implements UserDao, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private void setDataSOurce(DataSource dataSource) {
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}

	@Override
	public boolean isUser(String login) {
		
		String sql="SELECT COUNT(login) FROM users WHERE login='"+login+"';";
		int result=jdbcTemplate.queryForInt(sql);
		if(result>=1) return true;
		else return false;
	}

	@Override
	public User getUser(String login) {
		
		String SQL="SELECT * FROM users WHERE login='"+login+"';";
		return jdbcTemplate.queryForObject(SQL, new UserRowMapper());
		
	}

	@Override
	public List<User> getUsers() {
		
		String sql="SELECT * FROM users;";
		return jdbcTemplate.query(sql, new UserRowMapper());
	}
	private class UserRowMapper implements RowMapper<User> {
		

		@Override
		public User mapRow(ResultSet rs, int row) throws SQLException {
			
			User user=new User();
			user.setLogin(rs.getString("login"));
			user.setPassword(rs.getString("password"));
			user.setRole(rs.getString("role"));
			
			return user;
		}
		
	}

}
