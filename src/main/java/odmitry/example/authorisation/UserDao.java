package odmitry.example.authorisation;

import java.util.List;
/**
 * Interface for getting users
 * @author Freeman
 *
 */
public interface UserDao {
	
	public boolean isUser(String login);
	public User getUser(String login);
	public List<User> getUsers();

}
