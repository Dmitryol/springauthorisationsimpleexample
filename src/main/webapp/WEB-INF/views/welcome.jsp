<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>welcome</title>
</head>
<body>
     <h2>Table of users</h2>
     <table border="1">
     <tr>
         <th>LOGIN</th>
         <th>PASSWORD</th>
         <th>ROLE</th>
     </tr>
     <c:forEach items="${users}" var="user">
          <tr>
              <td>${user.login}</td>
              <td>${user.password}</td>
              <td>${user.role}</td>
          </tr>
     </c:forEach>
     </table>
     <br/>
     <a href="logout">logout</a>
</body>
</html>