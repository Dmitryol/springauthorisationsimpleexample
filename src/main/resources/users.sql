CREATE TABLE users(
login VARCHAR(25) PRIMARY KEY,
password VARCHAR(20) NOT NULL,
status VARCHAR(20)NOT NULL);

INSERT INTO users VALUES 
('admin','1234','ROLE_USER'),
('user','1111','ROLE_USER'),
('tester','3333','ROLE_USER');